// this file is explicitly using .js instead of .ts to avoid Express interface object to
// create error with the TS compiler it can be fixed using custom types
// more about here https://stackoverflow.com/questions/58957228/property-does-not-exist-on-type-requestparamsdictionary
// Dependencies
const express = require('express');
const morgan = require('morgan');
const app = express();
// Routing modules from Route folder
const taskRouter = require('./routes/taskRoutes');
const userRouter = require('./routes/userRoutes');

// MIDDLEWARES
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

app.use(express.json());
app.use(express.static(`${__dirname}/public`));

app.use((req, res, next) => {
  console.log('middleware ping test');
  next();
});

// ROUTES
app.use('/api/v1/tasks', taskRouter);
app.use('/api/v1/users', userRouter);

module.exports = app;
