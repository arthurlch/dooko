/* 
API features are implemented but not used.
Filtering, sorting etc will be done on the client side.
However if needed this features could be implement with from the server side 
// executing query using 
// EXECUTE QUERY
  const features = new APIFeatures(Task.find(), req.query)
    .filter()
    .sort()
    .field()
    .paginate();
  const tasks = await features.query;

  while it is bad to have non used code in your codebase, API Features filter have a very high chance to
  be implemented. Filtering, Pagination etc will be implemented client side.
  However this implementation can produce slow loading if too many items are treated. 
  

*/
class APIFeatures {
  // API FEATURES filter,pagination,sort etc
  constructor(query, queryString) {
    this.query = query;
    this.queryString = queryString;
  }
  filter() {
    // FILTERING
    const queryObj = { ...this.queryString };
    const excludedFields = ['page', 'sort', 'limit', 'fields'];
    excludedFields.forEach((element) => delete queryObj[element]);

    let queryStr = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);
    this.query = this.query.find(JSON.parse(queryStr));
    return this;
  }

  sort() {
    // SORTING
    if (this.queryString.sort) {
      const sortBy = this.queryString.sort.split(',').join(' ');
      this.query = this.query.sort(sortBy);
    } else {
      this.query = this.query.sort('__');
    }
    return this;
  }

  field() {
    // FIELD LIMITING
    if (this.queryString.fields) {
      const fields = this.queryString.fields(',').join(' ');
      this.query = this.query.select(fields);
    } else {
      this.query = this.query.select('-__v');
    }
    return this;
  }

  paginate() {
    // PAGINATION
    const page = this.queryString.page * 1 || 1;
    const limit = this.queryString.limit * 1 || 100;
    const skip = (page - 1) * limit;

    this.query = this.query.skip(skip).limit(limit);

    return this;
  }
}

module.exports = APIFeatures;
