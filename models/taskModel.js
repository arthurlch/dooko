// Task Schema
// This file is responsible of the production of the task model.
// Changes related to Task Schema/Model should be done in this file.
// DB = MongoDB / Mongoose Schema(https://mongoosejs.com/docs/guide.html)
const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Need to name Task!'],
    default: 'UNNAMED',
    minlength: [5, 'Name should be longer than 5 characters'],
    maxlength: [40, 'Name cannot be longer than 40 characters'],
  },
  index: {
    type: [],
  },
  identifier: {
    type: [String, Number],
    unique: true,
    default: 'A-00',
  }, // create unique indexes https://docs.mongodb.com/manual/core/index-unique/
  description: {
    type: String,
    trim: true,
    default: 'Enter description',
    minlength: [5, 'Description should be longer than 5 characters'],
    maxlength: [5000, 'Description cannot be longer than 5000 characters'],
  },
  reporter: { type: Object, default: undefined },
  createdAt: { type: Date, default: Date.now, select: false },
  status: {
    type: [String],
    required: [true, 'Status needed!'],
    default: 'Active',
    enum: [
      'Active',
      'In Progress',
      'On Track',
      'Delayed',
      'In Testing',
      'On Hold',
      'Approved',
      'Cancelled',
      'PLanning',
      'Completed',
      'Invoiced',
    ],
  },
  tags: {
    type: [String],
    enums: [''],
    required: [true, 'Tag needed!'],
    default: undefined,
  },
  due_date: { type: Date },
  severity: {
    type: [String],
    required: [true, 'Severity level needed!'],
    default: 'none',
    enum: ['None', 'Emergency', 'Critical', 'Major', 'Minor'],
  },
  archived: { type: Boolean, default: false },
  classification: {
    type: [String],
    required: [true, 'Classification needed!'],
    default: 'None',
    enum: [
      'None',
      'Security',
      'Crash',
      'Data loss',
      'Data breach',
      'Performance',
      'Network',
      'Social Engineering',
      'UI/UX',
      'Configuration',
      'Other',
    ],
  },
  reproducible: {
    type: [String],
    enum: [
      'None',
      'Always',
      'Sometimes',
      'Rarely',
      'Unable',
      'Undiscovered',
      'Unsuccesful',
    ],
  },

  comments: {
    type: [String],
    trim: true,
    default: undefined,
    minlength: [5, 'Comment should be longer than 5 characters'],
    maxlength: [500, 'Comment cannot be longer than 500 characters'],
  },
});

// Create Task model from task Schema
const Task = mongoose.model('Task', taskSchema);

module.exports = Task;
