const dotenv = require('dotenv');
const mongoose = require('mongoose');
// set config.env path
dotenv.config({ path: './config.env' });
const app = require('./app');
const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);
mongoose.connect(DB).then(() => console.log('DATABASE LOGIN SUCCESSFUL!'));
// port setup
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log('port test');
});
